require 'rails_helper.rb'

feature 'Deleting comments' do
  background do
    user = create :user
    @post = create(:post, user_id: user.id)
    @user_two = create(:user, email: 'ex2@example.com', user_name: 'usernum2')

    @comment = create(:comment, user_id: @user_two.id, post_id: @post.id)
    @comment_two = create(:comment, user_id: user.id,
                                    post_id: @post.id,
                                    content: 'blahblahblah')

    sign_in_with(@user_two)
  end

  scenario 'a user can delete their own comments' do
    visit '/'
    expect(page).to have_content('MyTextMyText')
    click_link "delete_#{@comment.id}"
    expect(page).to_not have_content('MyTextMyText')
  end

  scenario 'a user cannot delete someone elses comments via ui' do
    visit '/'
    expect(page).to have_content('blahblahblah')
    expect(page).to_not have_css("#delete_#{@comment_two.id}")
  end

  scenario 'a user cannot delete someone elses comments via url' do
    visit '/'
    expect(page).to have_content('blahblahblah')
    page.driver.submit :delete, "posts/#{@post.id}/comments/#{@comment_two.id}", {}
    expect(page).to have_content("That comment doesn't belong to you!")
    expect(page).to have_content('blahblahblah')
  end
end
