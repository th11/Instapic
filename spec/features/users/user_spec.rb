require 'rails_helper'

feature 'Creating a new user' do
  background do
    visit '/'
    click_link 'Register'
  end

  scenario 'can create a new user via index page' do
    fill_in 'user_email', with: 'example@example.com'
    fill_in 'user_user_name', with: 'example'
    fill_in 'user_password', with: '123123123'
    fill_in 'user_password_confirmation', with: '123123123'

    click_button 'Sign up'
    expect(page).to have_content('New Post')
  end

  scenario 'requires a user name to create an account' do
    fill_in 'user_email', with: 'example@example.com'
    fill_in 'user_password', with: '123123123'
    fill_in 'user_password_confirmation', with: '123123123'

    click_button 'Sign up'
    expect(page).to have_content("can't be blank")
  end

  scenario 'requires a user name to be at least 4 characters' do
    fill_in 'user_email', with: 'example@example.com'
    fill_in 'user_user_name', with: 'e'
    fill_in 'user_password', with: '123123123'
    fill_in 'user_password_confirmation', with: '123123123'

    click_button 'Sign up'
    expect(page).to have_content("is too short (minimum is 4 characters)")
  end

  scenario 'requires a user name to be less than or equal to 16 characters' do
    fill_in 'user_email', with: 'example@example.com'
    fill_in 'user_user_name', with: 'e' * 17
    fill_in 'user_password', with: '123123123'
    fill_in 'user_password_confirmation', with: '123123123'

    click_button 'Sign up'
    expect(page).to have_content("is too long (maximum is 16 characters)")
  end

end
