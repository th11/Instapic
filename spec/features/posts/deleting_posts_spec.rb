require 'rails_helper'

feature 'can delete a post' do
  background do
    @user = create(:user)
    sign_in_with(@user)

    post = create(:post, user_id: @user.id, caption: 'sample sample text')
    visit '/'
    find(:xpath, "//a[contains(@href,\"posts/#{post.id}\")]").click
    click_link 'Edit'
  end

  scenario 'Successfully deletes post' do
    click_link 'Delete'
    expect(page).to have_content('Post deleted.')
    expect(page).to_not have_content('sample sample text')
  end
end
