require 'rails_helper'

feature 'Index displays a list of posts' do
  background do
    @user = create(:user)
    sign_in_with(@user)
  end

  scenario 'the index displays correctly created posts' do
    post_one = create(:post, user_id: @user.id, caption: "This is post one")
    post_two = create(:post, user_id: @user.id, caption: "This is the second post")

    visit '/'
    expect(page).to have_content("This is post one")
    expect(page).to have_content("This is the second post")
    expect(page).to have_css("img[src*='coffee']")
  end
end
