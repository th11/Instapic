require 'rails_helper.rb'

feature 'Creating posts' do
  background do
    @user = create(:user)
    sign_in_with(@user)
  end

  scenario 'can create a post' do
    visit '/'
    click_link 'New Post'
    attach_file('post_image', "spec/files/images/coffee.png")
    fill_in 'post_caption', with: 'nom nom nom #coffeetime'
    click_button 'Create Post'
    expect(page).to have_content('#coffeetime')
    expect(page).to have_css("img[src*='coffee.png']")
    expect(page).to have_content(@user.user_name)
  end

  scenario 'require image for new post' do
    visit '/'
    click_link 'New Post'
    fill_in 'post_caption', with: 'some text'
    click_button 'Create Post'
    expect(page).to have_content('Failed to create post.')
  end
end
