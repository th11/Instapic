# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Post.destroy_all
User.destroy_all

u = User.create(email: 'th9382@gmail.com', password: ENV["MY_PASSWORD"], user_name: 'admin')

Post.create(user_id: u.id, caption: 'Pineapple', image: File.new("app/assets/images/doors.jpeg"))
Post.create(user_id: u.id, caption: 'Woman', image: File.new("app/assets/images/woman.jpeg"))
Post.create(user_id: u.id, caption: 'Liberty', image: File.new("app/assets/images/liberty.jpeg"))
Post.create(user_id: u.id, caption: 'Swing', image: File.new("app/assets/images/swing.jpeg"))
Post.create(user_id: u.id, caption: 'Phone', image: File.new("app/assets/images/phone.jpeg"))
Post.create(user_id: u.id, caption: 'Sun', image: File.new("app/assets/images/sun.jpg"))
Post.create(user_id: u.id, caption: 'Pineapple', image: File.new("app/assets/images/pineapple.jpeg"))
