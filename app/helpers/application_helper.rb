module ApplicationHelper
  FLASH_TYPES = {
    success: 'alert-success',
    error: 'alert-danger',
    alert: 'alert-warning',
    notice: 'alert-info'
  }

  def alert_for(flash_type)
    FLASH_TYPES[flash_type.to_sym] || flash_type.to_s
  end
end

def form_image_select(post)
  if post.image.exists?
    return image_tag post.image.url(:medium),
                     id: 'image-preview',
                     class: 'img-responsive'
  end

  image_tag 'placeholder.png', id: 'image-preview', class: 'img-responsive'
end
